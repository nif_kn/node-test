var main = function() {

    $('button').click(function(event) {
        var comment = $("textarea").val();

        if (comment != "") {
            var html = $('<li>').text(comment.substring(0,50));
            html.appendTo('#theOrder');
            $("textarea").val("");
        }

        return false;
    });

}

$(document).ready(main);